package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.printf("Let's play round %d%n", roundCounter);
            String humanChoice = validate_and_confirm();
            String computerChoice = choiceComputer();
            String choices = String.format("Human chose %s, computer chose %s. ", humanChoice, computerChoice);

            if (decide(humanChoice, computerChoice)) {
                System.out.println(choices + "Human wins!");
                humanScore++;
            }
            else if (decide(computerChoice, humanChoice)) {
                System.out.println(choices + "Computer wins!");
                computerScore++;
            }
            else {
                System.out.println(choices + "It's a tie!");
            }

            System.out.printf("Score: human %d, computer %d%n", humanScore, computerScore);
            String anotherRound = readInput("Do you wish to continue playing? (y/n)?");

            if (!anotherRound.equals("y")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter++;
        } 
    }

    public Boolean decide(String choice_1, String choice_2) { /**Forbedret egen kode fra tidligere, ved hjelp av vedlagt eksempel*/
        if (choice_1.equals("paper")) {
            return choice_2.equals("scissors");
        }
        else if (choice_1.equals("scissors")) {
            return choice_2.equals("paper");
        }
        else {
            return choice_2.equals("scissors");
        }
    }

    public String validate_and_confirm() {
        while (true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (!rpsChoices.contains(humanChoice)) {
                System.out.printf("I do not understand %s. Could you try again?%n", humanChoice);
            }
            else {
                return humanChoice;
            }
            }
    }

    public String choiceComputer() {
        Random randomChoice = new Random();
        String computerChoice = rpsChoices.get(randomChoice.nextInt(rpsChoices.size()));
        return computerChoice;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
